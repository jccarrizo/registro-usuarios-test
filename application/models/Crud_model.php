<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function load_users() {
        $this->db->select('users.id, users.name, users.last_name, users.email, users.password, municipios.nombre_muni, departamentos.nombre_depa, paises.nombre_pais');
        $this->db->from("users");
        $this->db->join('user_municipio', 'user_municipio.user_id = users.id');
        $this->db->join('municipios', 'municipios.id = user_municipio.municipio_id ');
        $this->db->join('departamentos', 'departamentos.id = municipios.departamento_id ');
        $this->db->join('paises', 'paises.id = departamentos.pais_id ');


        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }

    public function getUserById($id) {
        $this->db->select('users.id, users.name, users.last_name, users.email, users.password, '
                . 'municipios.nombre_muni, municipios.id as idmuni, departamentos.nombre_depa,'
                . ' departamentos.id as iddepa, paises.nombre_pais, paises.id as idpais');
        $this->db->from("users");
        $this->db->join('user_municipio', 'user_municipio.user_id = users.id');
        $this->db->join('municipios', 'municipios.id = user_municipio.municipio_id ');
        $this->db->join('departamentos', 'departamentos.id = municipios.departamento_id ');
        $this->db->join('paises', 'paises.id = departamentos.pais_id ');
        $this->db->where("users.id", $id);
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }

    public function getCountries() {
        $this->db->select("*");
        $this->db->from("paises");
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }

    public function departamentos($id_country = null) {
        $this->db->select("departamentos.id, departamentos.nombre_depa, departamentos.pais_id");
        $this->db->from("departamentos");
        $this->db->where("departamentos.pais_id", $id_country);
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }

    public function municipios($id_dep) {
        $this->db->select("municipios.id, municipios.nombre_muni, municipios.departamento_id");
        $this->db->from("municipios");
        $this->db->where("municipios.departamento_id", $id_dep);
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }

    public function remove_user_model($id) {
        if ($this->db->delete('users', array('id' => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update_user_model($id, $userdata, $id_municipio) {

        $this->db->trans_start();
        $this->db->where('id', $id);
        if ($this->db->update('users', $userdata)) {

            $this->db->where('user_id', $id);
            $municdata = array(
                "municipio_id" => $id_municipio
            );
            if ($this->db->update('user_municipio', $municdata)) {
                $this->db->trans_complete();
                return TRUE;
            } else {
                $this->db->trans_complete();
                return FALSE;
            }
        } else {
            $this->db->trans_complete();
            return FALSE;
        }
    }
    
    
    public function insert_user_model($userdata, $id_municipio) {

        $this->db->trans_start();
        if ($this->db->insert('users', $userdata)) {
            $id = $this->db->insert_id();            
            
            $municdata = array(
                "user_id" => $id,
                "municipio_id" => $id_municipio
            );
            if ($this->db->insert('user_municipio', $municdata)) {
                $this->db->trans_complete();
                return TRUE;
            } else {
                $this->db->trans_complete();
                return FALSE;
            }
        } else {
            $this->db->trans_complete();
            return FALSE;
        }
    }
    
    

    public function checkEmail($email) {
        $this->db->select("users.id");
        $this->db->from("users");
        $this->db->where("users.email", $email);
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return FALSE;
        }
        return TRUE;
    }
    public function checkEmailForUpdate($id,$email) {
        $this->db->select("users.id");
        $this->db->from("users");
        $this->db->where("users.email", $email);
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return FALSE;
        }
        $user = $raw->result();
        if($user[0]->id == $id){
            return FALSE;
        }
        return TRUE;
    }

}
