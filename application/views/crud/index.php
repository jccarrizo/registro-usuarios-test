<?php

?>


<div class="row">

    <div class="col-lg-12">
        <div class="col m-4">
            <h4>Tabla de usuarios</h4>
        </div>

        <div class="my-4">            
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Correo</th>
                        <th scope="col">País</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($users_datail):
                        foreach ($users_datail as $key => $data):
                            ?>
                            <tr>
                                <th scope="row"><?= ($key + 1); ?></th>
                                <td><?= $data->name; ?></td>
                                <td><?= $data->last_name; ?></td>
                                <td><?= $data->email; ?></td>
                                <td><?= $data->nombre_pais; ?></td>
                                <td><div class="btn-group"><a href="<?= base_url("crud/edit-user/$data->id"); ?>" class="btn btn-success btn-edit">Editar <li class="fa fa-edit" ></li> </a></div> <?php $disable=""; if($user_id === $data->id ): $disable = " disabled "; endif; ?><div class="btn-group"><button type="button" <?=$disable?>class="btn btn-danger btn-remove" value="<?= $data->id; ?>">Eliminar <li class="fa fa-minus-circle" ></li> </button></div>  <div class="btn-group"> <a href="<?= base_url("crud/view-user/$data->id"); ?>" class="btn btn-info btn-edit">Ver <li class="fa fa-eye" ></li> </a></div> </td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
            <div class="m-4"><a href="<?= base_url("crud/add-user"); ?>" id="add-user" class="btn btn-primary" style="float: right">Agregar <li class="fa fa-plus-circle"></li></a></div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user-title-msg"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div id="user-msg-msg" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




