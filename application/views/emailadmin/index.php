<?php
echo '<script>var _URLBASE = "'.base_url().'"</script>';
?>
<div class="col-lg-12">
    <h4 class="m-4">Administrador de Correos</h4>
    <hr>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 my-4">            
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">De:</th>
                            <th scope="col">Para:</th>
                            <th scope="col">Asunto</th>
                            <th scope="col">Mensaje</th>                            
                            <th scope="col">Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $available = "disabled";
                        if ($correos):
                            foreach ($correos as $key => $correo):
                            
                                $status = "En cola";
                                if ($correo->status == 1) {
                                    $status = "Enviado";
                                }else{
                                    $available = "";
                                }
                                ?>
                                <tr>
                                    <td><?= ($key+1) ?></td>
                                    <td><?= $correo->sender ?></td>
                                    <td><?= $correo->receiver ?></td>
                                    <td><?= $correo->subject ?></td>
                                    <td><?= $correo->message ?></td>
                                    <td><?= $status ?></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            echo '<h4 class="m-4">No hay correos en esta lista</h4>';
                        endif;
                        ?>
                    </tbody>
                </table>
                <div class="m-4"><a href="<?= base_url("mailer/new-email"); ?>" id="add-user" class="btn btn-primary" style="float: right">Nuevo Correo <i class="fa fa-envelope"></i></li></a> <button id="mails" <?= $available; ?> class="btn btn-warning">Enviar correos en cola</button></div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="email-title-msg"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div id="email-msg-msg" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

