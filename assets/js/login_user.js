(function ($) {
    var _REDIRECT;
    $("#user-modal").on('hide.bs.modal', function () {
        if (_REDIRECT) {
            if (_REDIRECT !== "") {
                window.location.replace(_REDIRECT);
            }
            _REDIRECT = "";
        }
    });


    function validate() {
        var valid = true;
        $("#email > input").removeClass('is-invalid');
        $("#email > div.invalid-feedback").html("");
        if ($("#email > input").val().length === 0) {
            $("#email > input").addClass('is-invalid');
            $("#email > div.invalid-feedback").html("¡Este campo es requerido!");
            valid = false;
        } else {
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            if (!testEmail.test($("#email > input").val())) {
                $("#email > input").addClass('is-invalid');
                $("#email > div.invalid-feedback").html("¡Correo invalido!");
                valid = false;
            }
        }

        $("#password > input").removeClass('is-invalid');
        $("#password > div.invalid-feedback").html("");
        if ($("#password > input").val().length === 0) {
            $("#password > input").addClass('is-invalid');
            $("#password > div.invalid-feedback").html("¡Este campo es requerido!");
            valid = false;
        } else if ($("#password > input").val().length < 5) {
            $("#password > input").addClass('is-invalid');
            $("#password > div.invalid-feedback").html("¡La contraseña debe contener al menos 5 caracteres!");
            valid = false;
        } else if ($("#password > input").val().length > 50) {
            $("#password > input").addClass('is-invalid');
            $("#password > div.invalid-feedback").html("¡La contraseña debe no debe exeder los 50 caracteres!");
            valid = false;
        }


        return valid;
    }

    $(document).ready(function () {
        $("#login-form").submit(function (ev) {
            ev.preventDefault();
            if (!validate()) {
                return;
            }
            var form = $(this);

            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: $(this).serialize(),
                beforeSend: function () {
                    $(".form-group > input").removeClass("is-invalid");
                    $(".alert").remove();
                },
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    if (obj.result !== undefined) {
                        if (obj.result === true) {
                            window.location.href = obj.url;
                        }
                    }
                },
                error: function (xhr) {
                    if (xhr.status === 400) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        if (obj.msg !== undefined) {
                            _REDIRECT = (obj.url);
                            $("#user-title-msg").html(obj.title);
                            $("#user-msg-msg").html(obj.msg);
                            $('#user-modal').modal('show');
                            $("#addUser-btn").removeAttr("disabled");
                        }
                    } else {
                        alert(xhr.status);
                    }
                },
                complete: function () {
                    form.find('button').removeAttr("disabled");
                }
            });

        });
    });

})(jQuery);