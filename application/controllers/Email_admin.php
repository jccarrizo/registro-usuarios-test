<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->is_logged) {
            redirect("login");
        }
        $this->load->model("Emailadmin_model"); //llamado para cargar el Modelo y poder usar sus metodos internos.
    }

    public function index() {
        $data = array();
        $this->load->helper("add_js_file"); //función para agregar un archivo javascript. este se cargara al final de body.
        insert_javascript(array("js/mails.js")); //el javascript que se inserta.
        //$data['users_datail'] = $this->Crud_model->load_users(); //carga de todos los usuarios
        $data['user_id'] = $this->session->userdata('id'); //id del usuario que ha iniciado sessión.
        //getMessages()
        $data['correos'] = $this->Emailadmin_model->getMessages();
        set_template('emailadmin/index', $data); //funcion para armar el template y la pagina que se mostrará. 
    }

    public function new_email_page() {
        $data = array();
        $this->load->helper("add_js_file"); //función para agregar un archivo javascript. este se cargara al final de body.
        insert_javascript(array("js/newemail.js")); //el javascript que se inserta.
        set_template('emailadmin/new_email', $data); //funcion para armar el template y la pagina que se mostrará. 
    }

    public function new_email() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        //	sender, receiver, subject, message, status. campos de la tabla
        $receiver = $this->input->post("receiver");
        $subject = $this->input->post("subject");
        $message = $this->input->post("message");
        $data = array(
            "sender_id" => $this->session->userdata('id'),
            "sender" => $this->session->userdata('email'),
            "receiver" => $receiver,
            "subject" => $subject,
            "message" => $message,
            "status" => 0,
        );
        if ($this->Emailadmin_model->new_email_model($data)) {
            $title = 'Hecho';
            $msg = '¡El mensaje se ha agregado a la cola';
            $url = base_url("mailer");
        } else {
            $this->output->set_status_header(400);
            $title = 'Error';
            $msg = '¡Ocurrio un error, intentelo más tarde!';
            $url = '';
        }
        $response = array('title' => $title, 'msg' => $msg, 'url' => $url);
        echo json_encode($response);
    }

    public function send_list_mails() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->model('Correo_modelo');
        $mails = $this->Correo_modelo->getListQueueMails();
        $mailCount = 0;
        $mailsSended = 0;
        if ($mails) {
            foreach ($mails as $mail) {
                $contacto = $mail->sender;
                $Asunto = $mail->subject;
                $correo_receiver = $mail->receiver;
                $Mensaje = $mail->message;
                $mailCount++;
                $id = $mail->id;
                if ($this->sendMail($correo_receiver, $Asunto, $Mensaje, $contacto)) {
                    $mailsSended++;
                    $this->Correo_modelo->updateListMails($id);
                }
            }
        }
        $title = 'Hecho';
        $msg = 'Correos enviados: ' . $mailCount . '. - Correos entregados: ' . $mailsSended;
        $url = base_url("mailer");
        $response = array('title' => $title, 'msg' => $msg, 'url' => $url);
        echo json_encode($response);
    }

    private function sendMail($correo_receiver, $Asunto, $Mensaje, $contacto) {
        $Nombre = $this->session->userdata('name');
        $Nombre = html_escape($Nombre);
        $Asunto = html_escape($Asunto);
        $Mensaje = html_escape($Mensaje);
        $password = "Ju4nc4rl0$987";
        $Asunto = htmlentities($Asunto, ENT_QUOTES, "UTF-8");
        $correo_sender = "jc.carrizoc79@gmail.com";
//        return;
        $config = array(
            "protocol" => "smtp",
            "smtp_host" => "ssl://smtp.googlemail.com",
            "smtp_port" => 465,
            "smtp_user" => $correo_sender,
            "smtp_pass" => $password,
            "mailtype" => "html",
            "charset" => "iso-8859-1",
            "wordwrap" => TRUE,
        );
        $body = $Nombre . ":\r\n" . $Mensaje . "\r\n Contacto " . $contacto;
        $body = htmlentities($body, ENT_QUOTES, "UTF-8");

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($correo_sender);
        $this->email->to($correo_receiver);
        $this->email->subject($Asunto);
        $this->email->message($body);
        if ($this->email->send()) {
            return TRUE;
        }
        return FALSE;
    }

}
