(function ($) {
    var _REDIRECT;
    $("#user-modal").on('hide.bs.modal', function () {
        if (_REDIRECT) {
            location.reload();
            _REDIRECT = "";
        }
    });
    $(document).ready(function () {
        $("#pais").change(function () {
            var id = $(this).find(":checked").val();
            //alert("Id: " + id);
            if (id !== "0") {
                $.ajax({
                    url: _URLBASE + "crud/getDepartamentos",
                    type: "POST",
                    data: {id_country: id}
                })
                        .done(function (data) {
                            var obj = jQuery.parseJSON(data);
                            $('#departamento_opt').html("<option value='0'>Elija una opción</option>");
                            for (var i = 0; i < obj.length; i++) {
                                var selected = "";
                                if (_PAISES[0]['pais_id'] === obj[i].id) {
                                    selected = "selected";
                                }
                                $('#departamento_opt').append("<option value='" + obj[i].id + "' " + selected + " >" + obj[i].nombre_depa + "</option>");
                            }
                        })
                        .fail(function (data) {
                            alert("error");
                        })
                        .always(function (data) {

                        });
            }

            if ($("#pais > select").val() === "0") {
                $("#municipios").hide();
                $("#departamento").hide();
            } else {
                $("#pais > select").removeClass('is-invalid');
                $("#pais > div.invalid-feedback").html("");
                $("#departamento").show();
                $("#municipios").hide();
            }

        });
        $("#departamento_opt").change(function () {
            var id = $(this).find(":checked").val();
            if (id !== "0") {
                $.ajax({
                    url: _URLBASE + "crud/getMunicipios",
                    type: "POST",
                    data: {id_dep: id}
                })
                        .done(function (data) {
                            var obj = jQuery.parseJSON(data);
                            $('#municipio_opt').html("<option value='0' selected >Elija una opción</option>");
                            for (var i = 0; i < obj.length; i++) {
                                $('#municipio_opt').append("<option value='" + obj[i].id + "' >" + obj[i].nombre_muni + "</option>");
                            }
                        })
                        .fail(function (data) {
                            alert("error");
                        })
                        .always(function (data) {

                        });
            }
            if ($("#departamento > select").val() === "0") {
                $("#municipios").hide();
            } else {
                $("#departamento > select").removeClass('is-invalid');
                $("#departamento > div.invalid-feedback").html("");
                $("#municipios").show();
            }
        });
        $("#municipio_opt").change(function () {
            if ($("#municipios > select").val() !== "0") {
                $("#municipios > select").removeClass('is-invalid');
                $("#municipios > div.invalid-feedback").html("");
            }
        });
    });
    function validate() {
        $("#name input[name=register-name]").removeClass('is-invalid');
        $("#name > div.invalid-feedback").html("");
        var valid = true;
        if ($("#name > input[name=register-name]").val().length === 0) {
            $("#name > input[name=register-name]").addClass('is-invalid');
            $("#name > div.invalid-feedback").html("¡Este campo es requerido!");
            valid = false;
        } else {
            if ($("#name > input[name=register-name]").val().length < 2) {
                $("#name > input[name=register-name]").addClass('is-invalid');
                $("#name > div.invalid-feedback").html("¡El Nombre debe contener al menos 2 caracteres!");
                valid = false;
            } else if ($("#name > input[name=register-name]").val().length > 50) {
                $("#name > input[name=register-name]").addClass('is-invalid');
                $("#name > div.invalid-feedback").html("¡El Nombre debe no debe exeder los 50 caracteres!");
                valid = false;
            }
        }
        $("#last-name input").removeClass('is-invalid');
        $("#last-name > div.invalid-feedback").html("");
        if ($("#last-name > input").val().length === 0) {
            $("#last-name > input").addClass('is-invalid');
            $("#last-name > div.invalid-feedback").html("¡Este campo es requerido!");
            valid = false;
        } else {
            if ($("#last-name > input").val().length < 2) {
                $("#last-name > input").addClass('is-invalid');
                $("#last-name > div.invalid-feedback").html("¡El Apellido debe contener al menos 2 caracteres!");
                valid = false;
            } else if ($("#last-name > input").val().length > 50) {
                $("#last-name > input").addClass('is-invalid');
                $("#last-name > div.invalid-feedback").html("¡El Apellido debe no debe exeder los 50 caracteres!");
                valid = false;
            }
        }
        $("#email > input").removeClass('is-invalid');
        $("#email > div.invalid-feedback").html("");
        if ($("#email > input").val().length === 0) {
            $("#email > input").addClass('is-invalid');
            $("#email > div.invalid-feedback").html("¡Este campo es requerido!");
            valid = false;
        } else {
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            if (!testEmail.test($("#email > input").val())) {
                $("#email > input").addClass('is-invalid');
                $("#email > div.invalid-feedback").html("¡Correo invalido!");
                valid = false;
            }
        }
        $("#password > input").removeClass('is-invalid');
        $("#password > div.invalid-feedback").html("");
        if ($("#password > input").val().length > 0) {
            if ($("#password > input").val().length < 5) {
                $("#password > input").addClass('is-invalid');
                $("#password > div.invalid-feedback").html("¡La contraseña debe contener al menos 5 caracteres!");
                valid = false;
            } else if ($("#password > input").val().length > 50) {
                $("#password > input").addClass('is-invalid');
                $("#password > div.invalid-feedback").html("¡La contraseña debe no debe exeder los 50 caracteres!");
                valid = false;
            }
        }

        $("#pais > select").removeClass('is-invalid');
        $("#pais > div.invalid-feedback").html("");
        if ($("#pais > select").val() === "0") {
            $("#pais > select").addClass('is-invalid');
            $("#pais > div.invalid-feedback").html("¡Seleccione una opción!");
            valid = false;
        }
        $("#departamento > select").removeClass('is-invalid');
        $("#departamento > div.invalid-feedback").html("");
        if ($("#departamento > select").val() === "0") {
            $("#departamento > select").addClass('is-invalid');
            $("#departamento > div.invalid-feedback").html("¡Seleccione una opción!");
            valid = false;
        }
        $("#municipios > select").removeClass('is-invalid');
        $("#municipios > div.invalid-feedback").html("");
        if ($("#municipios > select").val() === "0") {
            $("#municipios > select").addClass('is-invalid');
            $("#municipios > div.invalid-feedback").html("¡Seleccione una opción!");
            valid = false;
        }

        return valid;
    }
    $("#update-form").submit(function (ev) {
        ev.preventDefault();
        if (!validate()) {
            return;
        }
        var form_data = new FormData(document.getElementById("update-form"));
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $("#update-btn").attr('disabled', 'disabled');
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                if (obj.title !== undefined) {
                    //form.trigger("reset");
                    _REDIRECT = (obj.url);
                    $("#user-title-msg").html(obj.title);
                    $("#user-msg-msg").html(obj.msg);
                    $('#user-modal').modal('show');
                }

            },
            error: function (xhr) {
                var obj = jQuery.parseJSON(xhr.responseText);
                _REDIRECT = (obj.url);
                $("#user-title-msg").html(obj.title);
                $("#user-msg-msg").html(obj.msg);
                $('#user-modal').modal('show');
                $("#update-btn").removeAttr("disabled");
                if (obj.type === "email") {
                    $("#email > input").addClass('is-invalid');
                    $("#email > div.invalid-feedback").html(obj.msg);
                }
            },
            complete: function () {
                //location.reload();
            }
        });
    });
})(jQuery);