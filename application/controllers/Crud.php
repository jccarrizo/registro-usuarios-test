<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {

    private $user_id;

    public function __construct() {
        parent::__construct();
        if (!$this->session->is_logged) {
            redirect("login");
        }
        $this->user_id = $this->session->userdata('id');
        $this->load->model("Crud_model"); //llamado para cargar el Modelo y poder usar sus metodos internos.
    }

    public function index() {
        $data = array();
        $this->load->helper("add_js_file"); //función para agregar un archivo javascript. este se cargara al final de body.
        insert_javascript(array("js/crud.js")); //el javascript que se inserta.
        $data['users_datail'] = $this->Crud_model->load_users(); //carga de todos los usuarios
        $data['user_id'] = $this->user_id; //id del usuario que ha iniciado sessión.
        set_template('crud/index', $data); //funcion para armar el template y la pagina que se mostrará. 
    }

    public function remove_user() {//función para remover un usuario
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id = $this->input->post("id");
        if ($this->user_id == $id) {
            $title = 'Error';
            $msg = '¡No es posible eliminar al usuario que ha iniciado sesión!';
            $url = base_url('');
            $response = array('title' => $title, 'msg' => $msg, 'url' => $url);
            echo json_encode($response);
            return;
        }
        if ($this->Crud_model->remove_user_model($id)) {
            $title = 'Hecho';
            $msg = '¡El usuario a sido removido!';
            $url = base_url("");
        } else {
            $title = 'Error';
            $msg = '¡Ocurrio un error, intentelo más tarde!';
            $url = base_url('');
        }

        $response = array('title' => $title, 'msg' => $msg, 'url' => $url);
        echo json_encode($response);
    }

    public function edit_user_page() {//pagina para editar un usuario
        $id = $this->uri->segment(3);
        $data = array();
        if ($this->Crud_model->getUserById($id)) {
            $data['data_user'] = $this->Crud_model->getUserById($id);
        }
        //funciones para cargar los datos de paises, departamentos y municipios
        if ($countries = $this->Crud_model->getCountries()) {
            $data['countries'] = $countries;
        }
        $id_country = $data['data_user'][0]->idpais;
        if ($departamentos = $this->Crud_model->departamentos($id_country)) {
            $data['departamentos'] = $departamentos;
        }
        $id_depart = $data['data_user'][0]->iddepa;
        if ($municipios = $this->Crud_model->municipios($id_depart)) {
            $data['municipios'] = $municipios;
        }

        $this->load->helper("add_js_file");
        insert_javascript(array("js/crud_edit.js"));
        set_template('crud/edit_user', $data);
    }

    public function view_user_page() {//pagina para editar un usuario
        $id = $this->uri->segment(3);
        $data = array();
        if ($this->Crud_model->getUserById($id)) {
            $data['data_user'] = $this->Crud_model->getUserById($id);
        }
        //funciones para cargar los datos de paises, departamentos y municipios
        if ($countries = $this->Crud_model->getCountries()) {
            $data['countries'] = $countries;
        }
        if (isset($data['data_user'])) {
            $id_country = $data['data_user'][0]->idpais;
            if ($departamentos = $this->Crud_model->departamentos($id_country)) {
                $data['departamentos'] = $departamentos;
            }
            $id_depart = $data['data_user'][0]->iddepa;
            if ($municipios = $this->Crud_model->municipios($id_depart)) {
                $data['municipios'] = $municipios;
            }
        }

        /* $this->load->helper("add_js_file");
          insert_javascript(array("js/crud_edit.js")); */
        set_template('crud/view_user', $data);
    }

    public function getDepartamentos() {//función para cargar los departamentos por ajax
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id_country = $this->input->post("id_country");
        if ($this->Crud_model->departamentos($id_country)) {
            echo json_encode($this->Crud_model->departamentos($id_country));
        } else {
            echo null;
        }
    }

    public function getMunicipios() {//función para cargar los municipios por ajax
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id_dep = $this->input->post("id_dep");
        if ($this->Crud_model->municipios($id_dep)) {
            echo json_encode($this->Crud_model->municipios($id_dep));
        } else {
            echo null;
        }
    }

    public function edit_user() {//función que recibe los datos para editar un usuario por ajax.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $id = $this->input->post("id");
        $name = $this->input->post("register-name");
        $last_name = $this->input->post("update-lastname");
        $email = $this->input->post("update-email");
        //checkEmailForUpdate
        if ($this->Crud_model->checkEmailForUpdate($id, $email)) {
            $this->output->set_status_header(400);
            $type = "email";
            $title = 'Error';
            $msg = '¡El correo ya se encuentra registrado!';
            $url = "";
            $response = array('title' => $title, 'msg' => $msg, 'url' => $url, "type" => $type);
            echo json_encode($response);
            return;
        }



        $password = $this->input->post("update-password");

        $id_municipio = $this->input->post("municipios");
        if ($password) {
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $data = array(
                'name' => $name,
                'last_name' => $last_name,
                'email' => $email,
                'password' => $hash
            );
        } else {
            $data = array(
                'name' => $name,
                'last_name' => $last_name,
                'email' => $email
            );
        }



        if ($this->Crud_model->update_user_model($id, $data, $id_municipio)) {
            $title = 'Hecho';
            $msg = '¡Los datos se actualizaron satisfactoriamente';
            $url = base_url("");
        } else {
            $this->output->set_status_header(400);
            $title = 'Error';
            $msg = '¡Ocurrio un error, intentelo más tarde!';
            $url = base_url('');
        }

        $response = array('title' => $title, 'msg' => $msg, 'url' => $url);
        echo json_encode($response);
    }

    public function add_user_page() {//pagina para agregar un usuario.
        $data = array();
        if ($countries = $this->Crud_model->getCountries()) {
            $data['countries'] = $countries;
        }

        $this->load->helper("add_js_file");
        insert_javascript(array("js/crud_adduser.js"));
        set_template('crud/add_user', $data);
    }

    public function add_user() {//función que recibe los datos para agregar un usuario por ajax.
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $name = $this->input->post("register-name");
        $last_name = $this->input->post("register-lastname");
        $email = $this->input->post("register-email");
        if ($this->Crud_model->checkEmail($email)) {
            $this->output->set_status_header(400);
            $type = "email";
            $title = 'Error';
            $msg = '¡El correo ya se encuentra registrado!';
            $url = "";
            $response = array('title' => $title, 'msg' => $msg, 'url' => $url, "type" => $type);
            echo json_encode($response);
            return;
        }
        $password = $this->input->post("register-password");
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $id_municipio = $this->input->post("municipios");

        $data = array(
            'name' => $name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => $hash
        );

        if ($this->Crud_model->insert_user_model($data, $id_municipio)) {
            $title = 'Hecho';
            $msg = '¡Los datos se registraron satisfactoriamente!';
            $url = base_url("");
        } else {
            $this->output->set_status_header(400);
            $title = 'Error';
            $msg = '¡Ocurrio un error, intentelo más tarde!';
            $url = "";
        }

        $response = array('title' => $title, 'msg' => $msg, 'url' => $url);
        echo json_encode($response);
    }

}
