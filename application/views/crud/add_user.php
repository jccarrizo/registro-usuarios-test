<?php 
echo "<script>var _URLBASE = '" . base_url() . "'</script>";
?>
<div class="row">

    <div class="col-lg-6">
        <div class="my-4">            
            <?php
            $attributes = array('method' => 'POST', 'id' => 'adduser-form');
            echo form_open(base_url('crud/add_user/'), $attributes);
            ?>
            <div class="form-group" id="name">
                <label for="register-name">Nombre</label>
                <input type="text" name="register-name" class="form-control" id="register-name" aria-describedby="nameHelp" value="" placeholder="nombre">
                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" id="last-name">
                <label for="register-lastname">Apellido</label>
                <input type="text" name="register-lastname" class="form-control" id="register-lastname" aria-describedby="lastnameHelp" value="" placeholder="apellido">
                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" id="email">
                <label for="register-email">Correo Electrónico</label>
                <input type="email" name="register-email" class="form-control" id="register-email" aria-describedby="emailHelp" value="" placeholder="email">
                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" id="password">
                <label for="register-password">Contraseña</label>
                <input type="password" name="register-password" class="form-control" id="register-password" aria-describedby="passwordHelp" value="" placeholder="password">
                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" id="repassword">
                <label for="register-repassword">Repetir contraseña</label>
                <input type="password" name="register-repassword" class="form-control" id="register-repassword" aria-describedby="passwordHelp" value="" placeholder="password">
                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" id="pais">
                <label id="pais-select" for="update-password">País</label>
                <select class="form-control" >                    
                    <option value="0">Elija una opción</option>
                    <?php
                    if ($countries):
                        foreach ($countries as $country) {
                            $selected = "";
                            echo "<option value='$country->id'$selected>$country->nombre_pais</option>";
                        }
                        ?>
                    <?php endif; ?>
                </select>

                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" id="departamento" style="display: none" >
                <label for="departamento_opt">Departamento</label>
                <select id="departamento_opt" class="form-control" >                    
                    <option value="0">Elija una opciōn</option>                    
                </select>
                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" id="municipios" style="display: none">
                <label for="municipio_opt">Municipios</label>
                <select id="municipio_opt" name="municipios" class="form-control" >                    
                    <option value="0">Elija una opciōn</option>                   
                </select>
                <div class="invalid-feedback d-block">
                </div>
            </div>
            <div class="form-group" >
                <button id="addUser-btn" type="submit" class="btn btn-success">Guardar</button>
                <a href="<?= base_url(); ?>" class="btn btn-primary" >Regresar</a>
                <input type="hidden" name="id" value="">
            </div>
            <?= form_close() ?>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user-title-msg"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div id="user-msg-msg" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>