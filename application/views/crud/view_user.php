<?php
$paises = array();
if (isset($data_user)):
    foreach ($data_user as $data) {
        array_push($paises, ["pais_name" => $data->nombre_pais, "departamento_name" => $data->nombre_depa, "municipio_name" => $data->nombre_muni]);
    }
    unset($data_user[0]->password);
    ?>

    <div class="col-lg-12">
        <h4 class="m-4">Detalles</h4>
        <hr>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm border rounded m-2 p-2">
                    <label for="name"><b>Nombre:</b></label>
                    <div id="name"><?= $data_user[0]->name ?></div>
                </div>
                <div class="col-sm border rounded m-2 p-2">
                    <label for="last-name"><b>Apellido:</b></label>
                    <div id="last-name" ><?= $data_user[0]->last_name ?></div>
                </div>                
            </div>

            <div class="row">
                <div class="col-sm border rounded m-2 p-2">
                    <label for="email"><b>Correo electrónico:</b></label>
                    <div id="email"><?= $data_user[0]->email ?></div>
                </div>
                <div class="col-sm border rounded m-2 p-2">
                    <label for="pais"><b>Pais:</b></label>
                    <div id="pais"><?= $paises[0]['pais_name'] ?></div>
                </div>                
            </div>

            <div class="row">
                <div class="col-sm border rounded m-2 p-2">
                    <label for="departamento"><b>Departamento:</b></label>
                    <div id="departamento"><?= $paises[0]['departamento_name'] ?></div>
                </div>
                <div class="col-sm border rounded m-2 p-2">
                    <label for="municipio"><b>Minucipio:</b></label>
                    <div id="municipio"><?= $paises[0]['municipio_name'] ?></div>
                </div>                
            </div>

            <div class="row">
                <div class="col-sm  m-2 p-2">
                    <a href="<?= base_url(); ?>" class="btn btn-primary" >Regresar</a>
                </div>                               
            </div>
        </div>
    </div>

    <?php else:
    ?>
    <div class="row">
        <div class="col-lg-12">
            <h4 class="m-4">¡El usuario no se encuentra registrado en nuestra Base de Datos!</h4>
            <hr>
        </div>
    </div>

<?php endif; ?>

