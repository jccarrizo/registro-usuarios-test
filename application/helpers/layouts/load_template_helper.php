<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function set_template($page,$data = array()) {
    $ci = & get_instance();
    $ci->load->view('layouts/head', $data);
    $ci->load->view('layouts/body_start');
    //**Página**//
    $ci->load->view("$page");
    //**********//    
    $ci->load->view("layouts/footer"); 
    $ci->load->view("layouts/body_end"); 
}