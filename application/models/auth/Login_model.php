<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
    
    
    private $table;
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table = "users";
    }

    public function access($user) {

        $this->db->select('users.id,users.name,users.last_name,'
                . 'users.email,users.password');
        $this->db->from($this->table);
        $this->db->where(array("email" => $user['email']), 1);
        $raw = $this->db->get();
        if ($raw->num_rows() == 0) {
            return FALSE;
        }
        $data = $raw->row_array();
        if (!password_verify($user['password'], $data['password'])) {
            return FALSE;
        }
        unset($data['password']);
        if (is_array($data)) {
            $temp_data = array_merge($data, array('is_logged' => TRUE));
            $this->session->set_userdata($temp_data);
            return TRUE;
        }
        return FALSE;
    }

}
