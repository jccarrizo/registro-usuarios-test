<?php ?>

<div class="card bg-light mb-3 center-block" style="max-width: 30rem;">
    <div class="card-header">Iniciar Sesión</div>
    <div class="card-body">
        <h5 class="card-title"></h5>
        <?php
        $attributes = array('method' => 'POST', 'id' => 'login-form');
        echo form_open(base_url('login/access/'), $attributes);
        ?>

        <div class="form-group" id="email">
            <label for="login-email">Correo Electrónico</label>
            <input type="email" name="login-email" class="form-control" id="login-email" aria-describedby="emailHelp" placeholder="correo electrónico">
            <div class="invalid-feedback d-block">
            </div>
        </div>
        <div class="form-group" id="password">
            <label for="login-password">Contraseña</label>
            <input type="password" name="login-password" class="form-control" id="login-password" placeholder="contraseña">
            <div class="invalid-feedback d-block">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
        <?= form_close(); ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user-title-msg"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div id="user-msg-msg" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>