<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Correo_Controller
 *
 * @author Juan C. Carrizo
 */
class Correo_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Correo_modelo');
    }
    
    
    public function send_list_mails(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $mails = $this->Correo_modelo->getListQueueMails();
        foreach ($mails as $mail){
            $contacto = $mail->sender;
            $Asunto = $mail->subject;
            $correo_receiver = $mail->receiver;
            $Mensaje = $mail->message;
            $id = $mail->id;
            if(send($correo_receiver,$Asunto,$Mensaje,$contacto)){
                $this->Correo_modelo->updateListMails($id);
            }
        }        
    }

    private function send($correo_receiver,$Asunto,$Mensaje,$contacto) {
        
        
               
        $Nombre = $this->session->userdata('name');

        $Nombre = html_escape($Nombre);
        $Asunto = html_escape($Asunto);
        $Mensaje = html_escape($Mensaje);
        //$this->load->library("encrypter");
        //$password = $config[0]->password;
        //$password = $this->encrypter->decrypt($password); 
        $password = "password"; 
        $Asunto = htmlentities($Asunto, ENT_QUOTES, "UTF-8");
        $correo_sender = "testmail@gmail.com";

        $config = array(
            "protocol" => "smtp",
            "smtp_host" => "ssl://smtp.googlemail.com",
            "smtp_port" => 465,
            "smtp_user" => $correo_sender,
            "smtp_pass" => $password,
            "mailtype" => "html",
            "charset" => "iso-8859-1",
            "wordwrap" => TRUE,
        );        
        $body = $Nombre . ":\r\n" . $Mensaje . "\r\n Contacto ".$contacto ;
        $body = htmlentities($body, ENT_QUOTES, "UTF-8");
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($correo_sender);
        $this->email->to($correo_receiver);
        $this->email->subject($Asunto);
        $this->email->message($body);
        if ($this->email->send()) {            
            return TRUE;
        }        
        return FALSE;
    }

}
