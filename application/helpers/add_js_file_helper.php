<?php

defined('BASEPATH') OR exit('No direct script access allowed');
global $list_js;

function load_javascript() {
    $script = '<!-- Extra Javascript -->';
    global $list_js;
    if (is_array($list_js)) {
        foreach ($list_js as $item) {
            $script .= '<script type="text/javascript" src="' . base_url() . 'assets/' . $item . '"></script>';
        }
    } else {
        if ($list_js !== null) {
            $script .= '<script type="text/javascript" src="' . base_url() . 'assets/' . $list_js . '"></script>';
        }
    }
    return $script;
}

function insert_javascript($js_arr = null) {
    global $list_js;
    $list_js = $js_arr;
}
