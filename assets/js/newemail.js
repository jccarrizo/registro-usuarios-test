(function ($) {
    var _REDIRECT;
    $("#email-modal").on('hide.bs.modal', function () {
        if (_REDIRECT) {
            if (_REDIRECT !== "") {
                window.location.replace(_REDIRECT);
            }
            _REDIRECT = "";
        }
    });

    $(document).ready(function () {

        function validate() {
            var valid = true;

            $("#email > input").removeClass('is-invalid');
            $("#email > div.invalid-feedback").html("");
            if ($("#email > input").val().length === 0) {
                $("#email > input").addClass('is-invalid');
                $("#email > div.invalid-feedback").html("¡Este campo es requerido!");
                valid = false;
            } else {
                var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
                if (!testEmail.test($("#email > input").val())) {
                    $("#email > input").addClass('is-invalid');
                    $("#email > div.invalid-feedback").html("¡Correo invalido!");
                    valid = false;
                }
            }

            $("#subject input").removeClass('is-invalid');
            $("#subject > div.invalid-feedback").html("");
            if ($("#subject > input").val().length === 0) {
                $("#subject > input").addClass('is-invalid');
                $("#subject > div.invalid-feedback").html("¡Este campo es requerido!");
                valid = false;
            } else {
                if ($("#subject > input").val().length < 2) {
                    $("#subject > input").addClass('is-invalid');
                    $("#subject > div.invalid-feedback").html("¡Este campo debe contener al menos 2 caracteres!");
                    valid = false;
                } else if ($("#subject > input").val().length > 50) {
                    $("#subject > input").addClass('is-invalid');
                    $("#subject > div.invalid-feedback").html("¡Este campo no debe exeder los 50 caracteres!");
                    valid = false;
                }
            }
            
            $("#message input").removeClass('is-invalid');
            $("#message > div.invalid-feedback").html("");
            if ($("#message > textarea").val().length === 0) {
                $("#message > textarea").addClass('is-invalid');
                $("#message > div.invalid-feedback").html("¡Este campo es requerido!");
                valid = false;
            } else {
                if ($("#message > textarea").val().length < 2) {
                    $("#message > textarea").addClass('is-invalid');
                    $("#message > div.invalid-feedback").html("¡Este campo debe contener al menos 2 caracteres!");
                    valid = false;
                } else if ($("#message > textarea").val().length > 1000) {
                    $("#message > textarea").addClass('is-invalid');
                    $("#message > div.invalid-feedback").html("¡Este campo no debe exeder los 1000 caracteres!");
                    valid = false;
                }
            }

            return valid;
        }


        $("#newmail-form").submit(function (ev) {
            ev.preventDefault();
            if (!validate()) {
                return;
            }
            var form_data = new FormData(document.getElementById("newmail-form"));
            var form = $(this);

            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $("#send-btn").attr('disabled', 'disabled');
                },
                success: function (data) {

                    var obj = jQuery.parseJSON(data);
                    if (obj.title !== undefined) {
                        //form.trigger("reset");
                        _REDIRECT = (obj.url);
                        $("#email-title-msg").html(obj.title);
                        $("#email-msg-msg").html(obj.msg);
                        $('#email-modal').modal('show');
                    }

                },
                error: function (xhr) {
                    //alert(xhr.responseText);
                    $("#error").html(xhr.responseText);
                    var obj = jQuery.parseJSON(xhr.responseText);
                    _REDIRECT = (obj.url);
                    $("#email-title-msg").html(obj.title);
                    $("#email-msg-msg").html(obj.msg);
                    $('#email-modal').modal('show');
                    $("#send-btn").removeAttr("disabled");

                },
                complete: function () {
                    //location.reload();
                }
            });
        });


    });
})(jQuery);