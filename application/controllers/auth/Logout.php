<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {
    public function __construct(){
        parent::__construct();
    }
     public function index(){
        $data = $this->session->all_userdata();
        foreach ($data as $key=>$value){
            $this->session->unset_userdata($key);
        }
        $this->session->sess_destroy(); 
        redirect('login/');
    }

}
