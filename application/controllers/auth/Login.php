<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->model("auth/Login_model");
    }

    public function index() {
        $data = array();
        $this->load->helper("add_js_file");
        insert_javascript(array('js/login_user.js'));
        set_template('auth/login', $data);
    }

    public function access() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $email = $this->input->post("login-email");
        $password = $this->input->post("login-password");
        $user = array(
            "email" => $email,
            "password" => $password
        );
        if ($this->Login_model->access($user)) {
            $result = TRUE;
            $url = base_url("");
            $response = array('result' => $result, 'url' => $url);
            echo json_encode($response);
        } else {
            $this->output->set_status_header(400);
            $title = "Error";
            $url = ("");
            $msg = "¡Credenciales incorrectas!";
            $response = array('title' => $title, 'url' => $url, "msg" => $msg);
            echo json_encode($response);
        }
    }

}
