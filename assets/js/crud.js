(function ($) {

    var _REDIRECT;
    $("#user-modal").on('hide.bs.modal', function () {
        if (_REDIRECT) {
            //window.location.replace(_REDIRECT);
            location.reload();
            _REDIRECT = "";
        }
    });


   
    
    

    $(".btn-remove").click(function () {
        var id = this.value;
        var formData = new FormData();
        formData.append('id', id);
        $.ajax({
            url: "http://localhost/registro_usuarios/crud/remove_user",
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {

            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                if (obj.title !== undefined) {
                    //form.trigger("reset");
                    _REDIRECT = (obj.url);
                    $("#user-title-msg").html(obj.title);
                    $("#user-msg-msg").html(obj.msg);
                    $('#user-modal').modal('show');
                }
            },
            error: function (xhr) {
                alert(xhr);
                if (xhr.status === 400) {
                } else {
                    //alert(xhr);
                }
            },
            complete: function () {

            }
        });
    });



})(jQuery);