(function ($) {
    var _REDIRECT;
    $("#email-modal").on('hide.bs.modal', function () {
        if (_REDIRECT) {
            if (_REDIRECT !== "") {
                window.location.replace(_REDIRECT);
            }
            _REDIRECT = "";
        }
    });
    $(document).ready(function () {
        $("#mails").on('click', function () {
            $(this).attr('disabled', 'disabled');
            $.ajax({
                url: _URLBASE + "mailer/send_mails",
                type: "POST",
                data: {flag: 0}
            })
                    .done(function (data) {
                        var obj = jQuery.parseJSON(data);
                        if (obj.title !== undefined) {
                            _REDIRECT = (obj.url);
                            $("#email-title-msg").html(obj.title);
                            $("#email-msg-msg").html(obj.msg);
                            $('#email-modal').modal('show');
                        }
                    })
                    .fail(function (data) {
                        alert("error: " + data.responseText);
                    })
                    .always(function (data) {
                        
                    });
        });
    });
})(jQuery);