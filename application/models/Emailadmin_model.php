<?php

/**
 * Description of Emailadmin_model
 *
 * @author Juan C. Carrizo
 */
class Emailadmin_model extends CI_Model {

    private $table;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table = "mensajeria";
    }

    public function getMessages() {
        $this->db->select("*");
        $this->db->from($this->table);
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }

    public function new_email_model($data) {

        if ($this->db->insert($this->table, $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function send_list_mails() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $mails = $this->Correo_modelo->getListQueueMails();
        foreach ($mails as $mail) {
            $contacto = $mail->sender;
            $Asunto = $mail->subject;
            $correo_receiver = $mail->receiver;
            $Mensaje = $mail->message;
            $id = $mail->id;
            if (send($correo_receiver, $Asunto, $Mensaje, $contacto)) {
                $this->Correo_modelo->updateListMails($id);
            }
        }
    }

}
