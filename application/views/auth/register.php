<?php ?>

<div class="card bg-light mb-3 center-block" style="max-width: 30rem;">
    <div class="card-header">Registrar</div>
    <div class="card-body">
        <h5 class="card-title">Crea una cuenta</h5>
        <?php
        $attributes = array('method' => 'POST', 'id' => 'register-form');
        echo form_open(base_url('register/create-account/'), $attributes);
        ?>
        <div class="form-group" id="name">
            <label for="register-name">Nombre</label>
            <input type="text" name="register-name" class="form-control" id="register-name" aria-describedby="nameHelp" placeholder="nombre">
            <div class="invalid-feedback d-block">
            </div>
        </div>
        <div class="form-group" id="last_name">
            <label for="register-lastName">Apellido</label>
            <input type="text" name="register-lastName" class="form-control" id="register-lastName" aria-describedby="lastNameHelp" placeholder="apellido">
            <div class="invalid-feedback d-block">
            </div>
        </div>
        <div class="form-group" id="email">
            <label for="register-email">Correo Electrónico</label>
            <input type="email" name="register-email" class="form-control" id="register-email" aria-describedby="emailHelp" placeholder="correo electrónico">
            <div class="invalid-feedback d-block">
            </div>
        </div>
        <div class="form-group" id="password">
            <label for="register-password">Contraseña</label>
            <input type="password" name="register-password" class="form-control" id="register-password" placeholder="contraseña">
            <div class="invalid-feedback d-block">
            </div>
        </div>
        <div class="form-group" id="repassword">
            <label for="register-rePassword">Repetir Contraseña</label>
            <input type="password" name="register-rePassword" class="form-control" id="register-rePassword" placeholder="repetir contraseña">
            <div class="invalid-feedback d-block">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        <?= form_close(); ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="register-title-msg"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div id="register-msg-msg" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

