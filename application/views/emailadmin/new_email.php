<?php ?>
<div class="col-lg-12">
    <h4 class="m-4">Nuevo Correo</h4>
    <hr>
    <div class="container-fluid">
        <?php
        $attributes = array('method' => 'POST', 'id' => 'newmail-form');
        echo form_open(base_url('mailer/new_email/'), $attributes);
        ?>
        <div class="row">
            <div class="col-sm border rounded m-2 p-2">
                <label for="sender"><b>De:</b></label>
                <div class="border rounded p-2" id="name">
                    <?= $this->session->userdata('email'); ?>
                </div>
            </div>
            <div class="col-sm border rounded m-2 p-2">
                <div class="form-group" id="email">
                    <label for="register-email"><b>Para:</b></label>
                    <input type="email" name="receiver" class="form-control" id="register-email" aria-describedby="emailHelp" value="" placeholder="correo">
                    <div class="invalid-feedback d-block">
                    </div>
                </div>
            </div>                
        </div>

        <div class="row">
            <div class="col-sm border rounded m-2 p-2">
                <div class="form-group" id="subject">
                    <label for="register-subject"><b>Asunto</b></label>
                    <input type="text" name="subject" class="form-control" id="register-subject" aria-describedby="subjectHelp" value="" placeholder="asunto">
                    <div class="invalid-feedback d-block">
                    </div>
                </div>
            </div>
            <div class="col-sm border rounded m-2 p-2">
                <div class="form-group" id="message">
                    <label for="register-message"><b>Mensaje</b></label>
                    <textarea name="message" class="form-control" id="register-message" aria-describedby="messageHelp" value="" placeholder="mensaje"></textarea>
                    <div class="invalid-feedback d-block">
                    </div>
                </div>
            </div>               
        </div>
        <div class="row">

            <div class="col-sm  m-2 p-2">
                <button id="send-btn" type="submit" class="btn btn-success" >Enviar a cola <i class="fa fa-paper-plane"></i></button>
                <a href="<?= base_url('mailer'); ?>" class="btn btn-primary" >Regresar <i class="fa fa-arrow-left"></i></a>
            </div>                               
        </div>
        <?= form_close() ?>

    </div>
</div>
<div id="error"></div>

<!-- Modal -->
<div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="email-title-msg"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times</span>
                </button>
            </div>
            <div id="email-msg-msg" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
