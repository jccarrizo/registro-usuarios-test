<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Description of Correo_modelo
 *
 * @author Juan C. Carrizo
 */
class Correo_modelo extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function getListQueueMails(){
        $this->db->select("*");
        $this->db->from("mensajeria");
        $this->db->where(array("status"=>0));
        $raw = $this->db->get();
        if ($raw->num_rows() === 0) {
            return false;
        }
        return $raw->result();
    }
    
    
    public function updateListMails($id){
        $data = array(
            "status"=>1
        );
        $this->db->where(array("id"=>$id));
        $this->db->update("mensajeria", $data);
        
    }
    
    
    
}
